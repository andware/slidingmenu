package com.example.slidingmenudemo;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.slidingmenu.lib.SlidingMenu;

public class LeftOrRightActivity extends Activity {

	/**
	 * SlidingMenu 
	 * 
    viewAbove - a reference to the layout that you want to use as the above view of the SlidingMenu
    viewBehind - a reference to the layout that you want to use as the behind view of the SlidingMenu
    touchModeAbove - an enum that designates what part of the screen is touchable when the above view is showing. Margin means only the left margin. Fullscreen means the entire screen. Default is margin.
    behindOffset - a dimension representing the number of pixels that you want the above view to show when the behind view is showing. Default is 0.
    behindWidth - a dimension representing the width of the behind view. Default is the width of the screen (equivalent to behindOffset = 0).
    behindScrollScale - a float representing the relationship between the above view scrolling and the behind behind view scrolling. If set to 0.5f, the behind view will scroll 1px for every 2px that the above view scrolls. If set to 1.0f, the behind view will scroll 1px for every 1px that the above view scrolls. And if set to 0.0f, the behind view will never scroll; it will be static. This one is fun to play around with. Default is 0.25f.
    shadowDrawable - a reference to a drawable to be used as a drop shadow from the above view onto the below view. Default is no shadow for now.
    shadowWidth - a dimension representing the width of the shadow drawable. Default is 0.
    fadeEnabled - a boolean representing whether or not the behind view should fade when the SlidingMenu is closing and "un-fade" when opening
    fadeDegree - a float representing the "amount" of fade. 1.0f would mean fade all the way to black when the SlidingMenu is closed. 0.0f would mean do not fade at all.
    selectorEnabled - a boolean representing whether or not a selector should be drawn on the left side of the above view showing a selected view on the behind view.
    selectorDrawable - a reference to a drawable to be used as the selector NOTE : in order to have the selector drawn, you must call SlidingMenu.setSelectedView(View v) with the selected view. Note that this will most likely not work with items in a ListView because of the way that Android recycles item views.

	 */   
    
	/**
	 * SlidingMenu 
	 * 
	 * 
	 * **/
	private SlidingMenu menu;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		menu = new SlidingMenu(this);
		Button button = new Button(this);//初始化组件
		button.setText("toggle");//添加一个不同用于控制menu
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				menu.toggle();//滑动的方法
			}
		});
   
		
//		使用时请注意：menu菜单的视图通过menu.setMenu控制。而左侧的视图就是直接通过setContentView控制的。
		
		//主题布局
		RelativeLayout view = new RelativeLayout(this);
		LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);//使用viewchild的方式编写界面
		view.addView(button,lp);
		setContentView(view);//主视图

		menu.setMode(SlidingMenu.LEFT);//设置菜单的位置为左边
//		menu.setMode(SlidingMenu.RIGHT);//设置菜单的位置为右
		menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);//设置菜单滑动的样式
		menu.setShadowWidthRes(R.dimen.shadow_width);//设置阴影宽度
		menu.setShadowDrawable(R.drawable.shadow);//设置阴影样式
		menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		menu.setBehindWidth(200);//菜单宽度
		menu.setFadeDegree(0.35f); 
		menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
		menu.setMenu(R.layout.activity_main);//添加菜单

	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
		menu.setBehindWidth(200);//菜单宽度
		menu.setFadeDegree(0.35f);
	}
	


}
